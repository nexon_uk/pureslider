(function($) {
// You can use $() inside of this function

	var PureSlider = function(options)
	{
		var sanitiseOptions = function(params) {
			// need unique id in case more than one pureslider instance
			params.uniq_img_id = params.slide_id + '__';

			for (prop in params)
			{ 
				if (params[prop].length > 1) {
					if (prop.slice(-2) == "id" && params[prop][0] != "#") {
						if (params.hasOwnProperty(prop)) {
							params[prop] = "#" + params[prop];
						}
					}
					if (prop.slice(-5) == "class" && params[prop][0] != ".") {
						if (params.hasOwnProperty(prop)) {
							params[prop] = "." + params[prop];
						}
					}
				}
			}

			// id should be unique
			if (params.caption_wrap_id == "")
				params.caption_wrap_id = params.slide_id + '-caption';
		}

		var ie_version = function ie_version() {
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf("MSIE ");
		 // If Internet Explorer, return version number
			if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))     
				return(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
			return false;
		}

		var consoleLog = function(msg) {
			window.console && console.log(msg);
		}

		var defaults = {
			slide_id : "slider", 
			active_class : "ps-active",
			static_ms : 7000, 		// millisecs each image stays visible
			transient_ms : 800,		// millisecs transition between slides

			captions : false,
			caption_wrap_id : "ps-caption",

			fade_in_first_img : false,
			freeze_slider : false, 		// stops sliding whilst hovering over image
			freeze_class : ".ps-frozen",// class to apply to active dot if frozen	

			nav_dots : true,
			nav_dots_id : "", 			// use ID if more than one slider on same page
			nav_dots_class : "ps-dots",
			nav_dots_wrap_class : "ps-dot-wrap",
			nav_dot_numbers : false,

			nav_arrow_prev_id: "",
			nav_arrow_next_id: "",
			nav_arrows_beside_dots : true,
			nav_arrows : false,
			default_nav_prev : "&#x25c0", // also x2b9c, x2bc7, x2b05, x2b95
			default_nav_next : "&#x25b6", // also x2b9e, x2bc8, x27a1

			uniq_img_id: ""  // NOTE - non-user system parameter
		}
		var params = $.extend(this, defaults, options);
		sanitiseOptions(params);
	
		var images = $(this.slide_id).children('img');
		var that = this;
		var ie = ie_version();
		var frozen = false;
		

		// Id selector prefixes
		var prev_arrow_classes = ".ps-arrows, .ps-prev";
		var next_arrow_classes = ".ps-arrows, .ps-next";
		var prev_arrow_select = this.nav_wrap_id + prev_arrow_classes;
		var next_arrow_select = this.nav_wrap_id + next_arrow_classes;
		var activeClass = this.active_class.substring(1);
		var dots_class = this.nav_dots_class.substring(1);
		var dot_select;
		PureSlider.prototype.timerId = false;

		// Dot selector
		if (this.nav_dots)
			dot_select = this.nav_dots_wrap_class;

		// Image selector
		var current_img_selector  = this.slide_id + ' img'+ this.active_class;

		var init = function(args) {
	
			if (ie !== false && ie <= 9)
				ie = true;
			else
			{
				// older IE browsers don't know about CSS3,
				$(this.slide_id + ' img').css( {
					'transition':'opacity ' + that.transient_ms + 'ms'
				}).error(function() {
					$(this).css({visibility:"hidden"});	
				});
			}

			$(this.slide_id + ' img').each(function(index) 
			{
				// check that the id is unused
				var newId = that.uniq_img_id + ("00" + index).slice(-3);
				// skip past the "#"
				$(this).prop("id", newId.substring(1));
			});


			// initial image display
			//========================
			$("<style> "  + this.active_class + " { opacity:1 !important; } </style>").appendTo("head");

			// freeze slider section
			//========================
			if (this.freeze_slider)
			{
				$(this.slide_id).on("mouseenter", function() {
					frozen = true;
					var n = ordinal($(current_img_selector));
					if (that.freeze_class != "")
						$(dot_select).eq(n).addClass(that.freeze_class);
						
				});
				$(this.slide_id).on("mouseleave", function() {
					frozen = false;
					if (that.freeze_class != "")
						$(dot_select).removeClass(that.freeze_class);
				});
			}

			// captions section
			//===================
			if (params.captions)
			{
				var txt = $(this.uniq_img_id + '000').attr("alt");
				$(this.caption_wrap_id).append("<p>" + txt + "</p>");
			}

			// nav_dots section
			//===================
			if (params.nav_dots)
			{
				var bind_func = [];
				var prev = prev_arrow_classes.replace(/[^-A-Za-z0-9 ]/g,"");
				var next = next_arrow_classes.replace(/[^-A-Za-z0-9 ]/g,"");
				if (this.nav_arrows_beside_dots)
				{
					$(dot_select).append("<span class=\"" + prev + "\">" + 
						this.default_nav_prev + "</span>");
				}
				for (n = 0; n < $(this.slide_id + ' img').length; n++)
				{
					if (this.nav_dot_numbers)
						bind_func[n] = $(dot_select).append("<span class=" + 
							dots_class + ">" + (n+1) + "</span>");
					else
						bind_func[n] = $(dot_select).append("<span class=" + 
							dots_class + " />");
				}
				if (this.nav_arrows_beside_dots)
				{
					$(dot_select).append("<span class=\"" + next + "\">" + 
						this.default_nav_next + "</span>");
				}
				
				dotEventsOn(); // setup event handlers for dot clicks

				// make first dot "active"
				$(this.nav_dots_class).eq(0).addClass(activeClass);
			}

			// nav_arrows section
			//=====================
			if (params.nav_arrows || params.nav_arrows_beside_dots)
			{
				$(prev_arrow_select).on('click', $.proxy(slide, this, -1));
				$(next_arrow_select).on('click', $.proxy(slide, this, +1));
			}

			//==================
			// start slider up!
			//==================
			if (images.length > 1)
				timerId = setInterval(function() { slide(); }, params.static_ms);

			initMove();

		} // end init()

		// get ordinal value of active image
		//=====================================
		var ordinal = function(img) {
			return parseInt(img.prop("id").slice(-3), 10);
		}

		var dotEventsOff = function() {
			if (that.nav_dots && that.nav_dots_class)
				$(that.nav_dots_class).off('click');	
		}

		var dotEventsOn = function() {
			if (that.nav_dots)  
			{
				$(that.nav_dots_class).each(function(intIndex) {
					$(this).on('click', $.proxy(moveTo, this, intIndex));
				});
			}
		}
			
		//=====================================
		// used to get correct image after clicking one of the dots
		//=====================================
		var moveTo = function(idx)
		{
			dotEventsOff();

			clearInterval(timerId);
			var cur_img = $(current_img_selector);
			var new_img = $(params.uniq_img_id + ("00" + idx).slice(-3));

			if (new_img == undefined || !new_img.length)
				consoleLog('Cannot locate image ' + idx + ' to move to.');
			else if (cur_img != new_img)
				move(cur_img, new_img);
			timerId = setInterval(function() { slide(); }, params.static_ms);
			dotEventsOn();
		}

		// Initial move of first image 
		//===============================
		var initMove = function()
		{
			new_img = images.first();
			if (this.fade_in_first_img)
			{
				// console.info('initMove fade-in');
				setTimeout(function() {
					if (!new_img.length) {
						consoleLog("Cannot locate initial image");
						return -1;
					}
					new_img.addClass(activeClass);
					dotAndCaptionHandler(new_img);
					this.interval_id = setTimeout(function() { slide(); }, that.static_ms);
				}, 20);
			}
			else
			{ 
				//console.info('initMove base');
				new_img.addClass(activeClass);
				dotAndCaptionHandler(new_img);
			}
		}

		var move = function(cur_img, new_img)
		{
			if (!frozen && cur_img != new_img)
			{
				if (!ie) {
					new_img.one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", 
					function() { 
						cur_img.removeClass(activeClass);
					});
					new_img.addClass(activeClass);
				}
				else
				{
					new_img.addClass(activeClass);
					cur_img.removeClass(activeClass);
				}
				dotAndCaptionHandler(new_img);
			}
		}

		// handler for dot and caption output
		// ====================================
		var dotAndCaptionHandler = function(new_img) {
			$(that.nav_dots_class).removeClass(activeClass);
			if (params.captions)
			{
				var txt = new_img.attr("alt");
				if (typeof txt !== 'undefined') {
					$(that.caption_wrap_id + " p").text(txt);
				}
				else
					consoleLog('Not able to obtain image alt attribute');
			}
			$(that.nav_dots_class).eq(ordinal(new_img)).addClass(activeClass);
		}

		function slide(direction) {
			if (frozen) return;
			direction = typeof direction !== 'undefined' ? direction : 1;
			var cur_img = $(current_img_selector);
			if (cur_img.length)
			{
				// -ve direction means 'prev' - go back
				if (direction >= 0)
				{
					var new_img = cur_img.next(that.slide_id + ' img');
					if (!new_img.length)
						new_img = images.first();
				}
				// +ve direction means 'next' - forwards
				else
				{
					new_img = cur_img.prev(that.slide_id + ' img');
					if (!new_img.length)
						new_img = images.last();
				}
				move(cur_img, new_img);
			}	
			else {
				consoleLog('Cannot determine current image for ' + activeClass);
				initMove();
			}
		}
	
		// =================================
		//  Initialise and start the slider
		// =================================
		init.apply(this);
	}

// bind pureSlide to jQuery to avoid further global namespace pollution
   $.fn.pureSlider = function(options)
   {
      return this.each(function(key, value) {
         var e = $(this);

			// ensure Id option is this DOM object's Id
			options.id = e.attr("id");

         // check if the element already has a PureSlider instance
         if (e.PureSlider) return e.PureSlider;

         var psObject = new PureSlider(options);
         e.PureSlider = psObject;
      });
   };

})(jQuery);
