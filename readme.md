# PureSlider #
<<<<<<< HEAD
A configurable jQuery plugin with optional captions, navigation arrows  and navigation dots. Any contributions to this project are very welcome.
=======
A configurable jQuery plugin with optional captions, navigation arrows  and navigation dots.  Any contributions to this project are very welcome.
>>>>>>> 0909867ba71312850b312454a5b7d15fbd638a8c

## HTML for this demo
```
   <div id="slider_caption"></div>
   <div class="ps-dot-wrap"></div>
   <div id="slider" class="ps-slider ps-responsive">
      <img src="boats.jpg" alt="Sailing boats at a regatta" />
      <img src="lightHouse.jpg" alt="Lighthouse - view near entrance" />
      <img src="lightHouse2.jpg" alt="Lighthouse - view from sea" />
   </div>
   </div>
```

## Default Options ##
PureSlider object defaults:

```javascript
	var defaults = {
         slide_id : "slider",
         active_class : "ps-active",
         static_ms : 8000,       // millisecs each image stays visible
         transient_ms : 500,     // millisecs transition between slides
         max_width: 0,

         captions : false,
         caption_id : "slider_caption",

         fade_in_first_img : false,
         freeze_slider : false,  // stops sliding whilst hovering over image
         freeze_class : "",   // class to apply to active dot if frozen 

         nav_dots : true,		// set to false for no navigation dots
         nav_dots_class : "ps-dots",
         nav_dots_wrap_class : "ps-dot-wrap",

         nav_arrows_beside_dots : false,
         nav_arrows : false,
         default_nav_next : ">",
         default_nav_prev : "<",
         nav_arrow_left_id: "",
         nav_arrow_right_id: ""
	};
```

## Example usage: 

With nav dots and no captions (the default is no captions):

```javascript
<script>
$(function() {
   $("#slider").pureSlider({ slide_id:"slider", nav_wrap_id:"dot_wrap" });
});
```

With nav dots *and* captions and navigation arrow set:
```javascript

<script>
$(function() {
	var sliderOptions = {
		slide_id : "slider", 
		captions : true,
		caption_wrap_id : "slider_caption",
		nav_wrap_id : "dot_wrap",
		default_nav_next : ">",
		default_nav_prev : "<"
	};
	$("#slider").pureSlider(sliderOptions);
```
